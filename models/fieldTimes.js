'use strict';
const {
  Model
} = require('sequelize');
const { v4 } = require('uuid');
module.exports = (sequelize, DataTypes) => {
  class fieldTimes extends Model {
    static associate(models) {
      this.belongsTo(models.fields)
      this.hasMany(models.bookings)
    }
  };
  fieldTimes.init({
    id: {
      type: DataTypes.UUID,
      primaryKey: true,
      defaultValue: v4
    },
    fieldId: {
      type: DataTypes.UUID,
      allowNull: false
    },
    startDateTime: {
      allowNull: false,
      type: DataTypes.DATE
    },
    endDateTime: {
      allowNull: false,
      type: DataTypes.DATE
    }
  }, {
    sequelize,
    modelName: 'fieldTimes',
    timestamps: false
  });
  return fieldTimes;
};