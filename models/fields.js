'use strict';
const {
  Model
} = require('sequelize');
const { v4 } = require('uuid');
module.exports = (sequelize, DataTypes) => {
  class fields extends Model {
    static associate(models) {
      this.belongsTo(models.locations)
      this.hasMany(models.fieldTimes)
      this.hasMany(models.fieldPhotos)
      this.hasMany(models.reviews)
      this.hasMany(models.reviews, {as: "ratingAvg"})
    }
  };
  fields.init({
    id: {
      type: DataTypes.UUID,
      primaryKey: true,
      defaultValue: v4
    },
    name: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    description: {
      type: DataTypes.TEXT,
      allowNull: true,
    },
    price: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    address: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    addressUrl: {
      type: DataTypes.STRING,
      allowNull: false,
    }
  }, {
    sequelize,
    modelName: 'fields',
    timestamps: false
  });
  return fields;
};