'use strict';
const {
  Model
} = require('sequelize');
const { v4 } = require('uuid');
module.exports = (sequelize, DataTypes) => {
  class bookings extends Model {
    static associate(models) {
      this.belongsTo(models.users)
      this.belongsTo(models.fieldTimes)
      this.hasOne(models.tickets)
      this.hasMany(models.userRentLists)
      this.hasMany(models.transactions)
    }
  };
  bookings.init({
    id: {
      type: DataTypes.UUID,
      primaryKey: true,
      defaultValue: v4
    },
    userId: {
      type: DataTypes.UUID,
      allowNull: false
    },
    fieldTimeId: {
      type: DataTypes.UUID,
      allowNull: false
    },
    createdAt: {
      allowNull: false,
      type: DataTypes.DATE
    },
    updatedAt: {
      allowNull: false,
      type: DataTypes.DATE
    },
    startTime: {
      allowNull: false,
      type: DataTypes.DATE,
      validation: {
        isDate: true
      },
    },
    endTime: {
      allowNull: false,
      type: DataTypes.DATE,
      validation: {
        isDate: true
      },
    },
    isSuccessful: {
      allowNull: false,
      type: DataTypes.BOOLEAN,
      defaultValue: false
    },
    status: {
      allowNull: false,
      type: DataTypes.TEXT,
      defaultValue: "waiting for payment"
    }
  }, {
    sequelize,
    modelName: 'bookings',
  });
  return bookings;
};