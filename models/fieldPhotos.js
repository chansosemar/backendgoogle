'use strict';
const {
  Model
} = require('sequelize');
const { v4 } = require('uuid');
module.exports = (sequelize, DataTypes) => {
  class fieldPhotos extends Model {
    static associate(models) {
      this.belongsTo(models.fields)
    }
  };
  fieldPhotos.init({
    id: {
      type: DataTypes.UUID,
      primaryKey: true,
      defaultValue: v4
    },
    fieldId: {
      type: DataTypes.UUID,
      allowNull: false
    },
    photoUrl: {
      type: DataTypes.STRING,
      allowNull: false,
    }
  }, {
    sequelize,
    modelName: 'fieldPhotos',
    timestamps: false
  });
  return fieldPhotos;
};