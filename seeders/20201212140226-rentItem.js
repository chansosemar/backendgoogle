'use strict';

const { v4 } = require("uuid");

module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.bulkInsert('rentItems', [{
      id: '28842a19-b57b-4bcc-ba8c-6ccd64406c34',
      name: 'Socks',
      price: '5000'
    },
    {
      id: '47302d5a-12c3-4f32-8cd5-56e5b95daf50',
      name: 'Shoes',
      price: '15000'
    },
    {
      id: '1e31d25a-d9f0-4905-bbcd-df75873c5965',
      name: 'Vest',
      price: '10000'
    }
    ])
  },

  down: async (queryInterface, Sequelize) => {
    await queryInterface.bulkDelete('rentItems', {});
  }
};
