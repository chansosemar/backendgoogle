'use strict';

// const { v4 } = require("uuid");

module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.bulkInsert('fieldTimes', [{
      id: '8619a5be-e6e1-48e1-b996-dadcda102579',
      fieldId: '0609e614-6ea7-478c-a73a-fa7b3bcff16c',
      startDateTime: new Date(1606494563000),
      endDateTime: new Date(1609000163000)
    },
    {
      id: 'c033126a-f8c6-41ad-b4a5-f8df6f3b0a9b',
      fieldId: '2a0f4ead-263e-4a80-a39e-fb4213917602',
      startDateTime: new Date(1606494563000),
      endDateTime: new Date(1609000163000)
    },
    {
      id: 'ee530b9a-0d87-4dc4-a8bd-054f982cde94',
      fieldId: '427e17b9-8020-44e9-a08a-c8aefdcd15ff',
      startDateTime: new Date(1606494563000),
      endDateTime: new Date(1609000163000)
    },
    {
      id: 'bb3275f1-ef43-4842-af12-7e3734f7d00c',
      fieldId: '45fad555-1dc1-4379-99db-0fbe349caf0e',
      startDateTime: new Date(1606494563000),
      endDateTime: new Date(1609000163000)
    },
    {
      id: '58938526-c712-41bd-ab54-61134467796d',
      fieldId: '669769e7-fdd9-49e1-8336-9567f7de3a19',
      startDateTime: new Date(1606494563000),
      endDateTime: new Date(1609000163000)
    },
    {
      id: '81842e31-72dd-40d8-9ac2-99d28cf9baf2',
      fieldId: '6a84a2dc-4719-47bb-8e83-279148592004',
      startDateTime: new Date(1606494563000),
      endDateTime: new Date(1609000163000)
    },
    {
      id: '11de72f7-cc7a-46a4-9929-da52c340e891',
      fieldId: '7037d828-0cf2-43f9-9d6d-4b53509882df',
      startDateTime: new Date(1606494563000),
      endDateTime: new Date(1609000163000)
    },
    {
      id: 'f0116ff5-8532-4b90-ab8a-77ac61526cf9',
      fieldId: '7f5b3efd-f157-4e3e-8db4-39d923ab7271',
      startDateTime: new Date(1606494563000),
      endDateTime: new Date(1609000163000)
    },
    {
      id: '7844578a-bf1e-456a-be03-d9612bb6229b',
      fieldId: '94e5e24f-1e10-4c68-aa93-8ffd34d8fe7c',
      startDateTime: new Date(1606494563000),
      endDateTime: new Date(1609000163000)
    },
    {
      id: '36d7f1c6-f159-4105-b6bb-d900c52ccd64',
      fieldId: 'c4bf55da-3c2c-41c6-8e75-415cf0864986',
      startDateTime: new Date(1606494563000),
      endDateTime: new Date(1609000163000)
    },
    {
      id: '58032a5b-0d0e-4ae8-b211-efb084611208',
      fieldId: 'e222f356-5caa-4de8-9962-669291dcb0f3',
      startDateTime: new Date(1606494563000),
      endDateTime: new Date(1609000163000)
    },
    {
      id: '7bdb1d36-0beb-469c-b116-0f3e8a874b7c',
      fieldId: 'eb0feb5c-cab9-42ca-8673-7fa7b5845b18',
      startDateTime: new Date(1606494563000),
      endDateTime: new Date(1609000163000)
    }
    ])
  },

  down: async (queryInterface, Sequelize) => {
    await queryInterface.bulkDelete('fieldTimes', {});

  }
};