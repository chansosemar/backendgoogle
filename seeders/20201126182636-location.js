'use strict';

// const { v4 } = require("uuid");

module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.bulkInsert('locations', [{
      id: '8174abc9-49d3-4f02-a35c-544be19f5b31',
      location: 'Bekasi Utara'
    },
    {
      id: '846bc366-fc1e-41e2-90f4-23a89bd195e8',
      location: 'Bekasi Timur'
    },
    {
      id: '62451a94-1138-4cbb-b54d-fce4e94c2004',
      location: 'Bekasi Selatan'
    },
    {
      id: '28c510c3-cde7-4f97-ad3f-9e9a5f613eac',
      location: 'Bekasi Barat'
    }
    ])
  },

  down: async (queryInterface, Sequelize) => {
    await queryInterface.bulkDelete('locations', {});

  }
};

