'use strict';

// const { v4 } = require("uuid");

module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.bulkInsert('fields', [{
      id: 'c4bf55da-3c2c-41c6-8e75-415cf0864986',
      name: 'Estadio Futsal',
      description: 'ESTADIO FUTSAL dibangun oleh para penggila futsal yang ingin memberikan kenikmatan dan kepuasan permainan kepada para pencinta futsal di kota Bekasi dan sekitar nya. Konsep eco-friendly dan ramah lingkungan telah menjadikan Estadio Futsal sebagai tempat bermain futsal yang sangat mendukung dan baik.',
      price: '120000',
      address: 'Jl. Perjuangan No.66, RT.003/RW.008, Marga Mulya, Kec. Bekasi Utara, Kota Bks, Jawa Barat 17143',
      addressUrl: 'https://goo.gl/maps/o25wS7GeFkbUNBQv7',
      locationId: '8174abc9-49d3-4f02-a35c-544be19f5b31'
    },
    {
      id: '94e5e24f-1e10-4c68-aa93-8ffd34d8fe7c',
      name: 'Prima Futsal',
      description: 'Melayani sewa lapangan futsalayanan yang terbaik.Fasilitas lengkap dan tentunya harga sewa lapangan futsal di sini juga sangat bersahabat.Usaha kami mampu bersaing dengan usaha usaha lain yang sejenis dengan kami. Tapi tentunya kami yang terbaik. Kami mengedepankan kualitas dan kepuasan pelanggan.Tempat kami juga strategis mudah dicari dan selalu diingat.',
      price: '110000',
      address: 'Jl. Perjuangan No.6, RT.001/RW.004, Harapan Baru, Kec. Bekasi Utara, Kota Bks, Jawa Barat 17123',
      addressUrl: 'https://goo.gl/maps/fYBhnwaPZRR4Qu4q7',
      locationId: '8174abc9-49d3-4f02-a35c-544be19f5b31'
    },
    {
      id: '45fad555-1dc1-4379-99db-0fbe349caf0e',
      name: 'Viva Futsal',
      description: 'Melayani sewa lapangan futsal dengan kualitas yang bagus.Dengan harga yang terjangkau dan pelayanan yang terbaik.Fasilitas lengkap dan tentunya harga sewa lapangan futsal di sini juga sangat bersahabat.Usaha kami mampu bersaing dengan usaha usaha lain yang sejenis dengan kami. Tapi tentunya kami yang terbaik. Kami mengedepankan kualitas dan kepuasan pelanggan.Tempat kami juga strategis mudah dicari dan selalu diingat.',
      price: '100000',
      address: 'Nomor 65 Kelurahan, Jl. Perjuangan, RT.003/RW.009, Marga Mulya, Kec. Bekasi Utara, Kota Bks, Jawa Barat 17142',
      addressUrl: 'https://goo.gl/maps/9Kw91CbZ8QvPS1Eh9',
      locationId: '8174abc9-49d3-4f02-a35c-544be19f5b31'
    },
    {
      id: 'eb0feb5c-cab9-42ca-8673-7fa7b5845b18',
      name: 'Champion Futsal',
      description: 'Melayani sewa lapangan futsal dengan kualitas yang bagus.Dengan harga yang terjangkau dan pelayanan yang terbaik.Fasilitas lengkap dan tentunya harga sewa lapangan futsal di sini juga sangat bersahabat.Usaha kami mampu bersaing dengan usaha usaha lain yang sejenis dengan kami. Tapi tentunya kami yang terbaik. Kami mengedepankan kualitas dan kepuasan pelanggan.Tempat kami juga strategis mudah dicari dan selalu diingat.',
      price: '125000',
      address: 'Jl. Ir. H. Juanda No.99 Bekasi Timur, Kota Bekasi Jawa Barat 17113',
      addressUrl: 'https://goo.gl/maps/zWsJbn8iZBhY1dVS7',
      locationId: '846bc366-fc1e-41e2-90f4-23a89bd195e8'
    },
    {
      id: '0609e614-6ea7-478c-a73a-fa7b3bcff16c',
      name: 'Planet Futsal',
      description: 'Melayani sewa lapangan futsal dengan kualitas yang bagus.Dengan harga yang terjangkau dan pelayanan yang terbaik.Fasilitas lengkap dan tentunya harga sewa lapangan futsal di sini juga sangat bersahabat.Usaha kami mampu bersaing dengan usaha usaha lain yang sejenis dengan kami. Tapi tentunya kami yang terbaik. Kami mengedepankan kualitas dan kepuasan pelanggan.Tempat kami juga strategis mudah dicari dan selalu diingat.',
      price: '130000',
      address: 'Jl. Chairil Anwar No.27, RT.004/RW.009, Margahayu, Kec. Bekasi Tim., Kota Bks, Jawa Barat 17113',
      addressUrl: 'https://goo.gl/maps/SEufxkjFF5fqc2Jp6',
      locationId: '846bc366-fc1e-41e2-90f4-23a89bd195e8'
    },
    {
      id: '7f5b3efd-f157-4e3e-8db4-39d923ab7271',
      name: 'Santoso Futsal',
      description: 'Melayani sewa lapangan futsal dengan kualitas yang bagus.Dengan harga yang terjangkau dan pelayanan yang terbaik.Fasilitas lengkap dan tentunya harga sewa lapangan futsal di sini juga sangat bersahabat.Usaha kami mampu bersaing dengan usaha usaha lain yang sejenis dengan kami. Tapi tentunya kami yang terbaik. Kami mengedepankan kualitas dan kepuasan pelanggan.Tempat kami juga strategis mudah dicari dan selalu diingat.',
      price: '120000',
      address: 'Jl. Pahlawan, RT.002/RW.017, Duren Jaya, Kec. Bekasi Tim., Kota Bks, Jawa Barat 17111',
      addressUrl: 'https://goo.gl/maps/HZgPUedy5FSiT1Pd8',
      locationId: '846bc366-fc1e-41e2-90f4-23a89bd195e8'
    },
    {
      id: '2a0f4ead-263e-4a80-a39e-fb4213917602',
      name: 'Futsal Town',
      description: 'Melayani sewa lapangan futsal dengan kualitas yang bagus.Dengan harga yang terjangkau dan pelayanan yang terbaik.Fasilitas lengkap dan tentunya harga sewa lapangan futsal di sini juga sangat bersahabat.Usaha kami mampu bersaing dengan usaha usaha lain yang sejenis dengan kami. Tapi tentunya kami yang terbaik. Kami mengedepankan kualitas dan kepuasan pelanggan.Tempat kami juga strategis mudah dicari dan selalu diingat.',
      price: '115000',
      address: 'Gang Hj Didi, Jl. Kemandoran, RT.003/RW.022, Pekayon Jaya, Kec. Bekasi Sel., Kota Bks, Jawa Barat 17148',
      addressUrl: 'https://goo.gl/maps/4JgzBCgWTMHSKPVk6',
      locationId: '62451a94-1138-4cbb-b54d-fce4e94c2004'
    },
    {
      id: '669769e7-fdd9-49e1-8336-9567f7de3a19',
      name: 'Jody Futsal',
      description: 'Melayani sewa lapangan futsal dengan kualitas yang bagus.Dengan harga yang terjangkau dan pelayanan yang terbaik.Fasilitas lengkap dan tentunya harga sewa lapangan futsal di sini juga sangat bersahabat.Usaha kami mampu bersaing dengan usaha usaha lain yang sejenis dengan kami. Tapi tentunya kami yang terbaik. Kami mengedepankan kualitas dan kepuasan pelanggan.Tempat kami juga strategis mudah dicari dan selalu diingat.',
      price: '110000',
      address: 'Jl. Cikunir Raya No.8, RT.001/RW.002, Jaka Mulya, Kec. Bekasi Sel., Kota Bks, Jawa Barat 17146',
      addressUrl: 'https://goo.gl/maps/2tgUyE6mLzXYtkMe9',
      locationId: '62451a94-1138-4cbb-b54d-fce4e94c2004'
    },
    {
      id: '6a84a2dc-4719-47bb-8e83-279148592004',
      name: 'Pulo Ribung Futsal',
      description: 'Melayani sewa lapangan futsal dengan kualitas yang bagus.Dengan harga yang terjangkau dan pelayanan yang terbaik.Fasilitas lengkap dan tentunya harga sewa lapangan futsal di sini juga sangat bersahabat.Usaha kami mampu bersaing dengan usaha usaha lain yang sejenis dengan kami. Tapi tentunya kami yang terbaik. Kami mengedepankan kualitas dan kepuasan pelanggan.Tempat kami juga strategis mudah dicari dan selalu diingat.',
      price: '120000',
      address: '22, Jl. Pensula 1, RT.004/RW.021, Jaka Setia, Bekasi Selatan, Bekasi City, West Java 17147',
      addressUrl: 'https://goo.gl/maps/C9ANk6ZTbpBqNf2G7',
      locationId: '62451a94-1138-4cbb-b54d-fce4e94c2004'
    },
    {
      id: 'e222f356-5caa-4de8-9962-669291dcb0f3',
      name: 'Bellarena Futsal',
      description: 'Melayani sewa lapangan futsal dengan kualitas yang bagus.Dengan harga yang terjangkau dan pelayanan yang terbaik.Fasilitas lengkap dan tentunya harga sewa lapangan futsal di sini juga sangat bersahabat.Usaha kami mampu bersaing dengan usaha usaha lain yang sejenis dengan kami. Tapi tentunya kami yang terbaik. Kami mengedepankan kualitas dan kepuasan pelanggan.Tempat kami juga strategis mudah dicari dan selalu diingat.',
      price: '130000',
      address: 'Jl. Bintara Jaya IV Gg. Pojok No.8, RT.012/RW.009, Bintara, Kec. Bekasi Bar., Kota Bks, Jawa Barat 17136',
      addressUrl: 'https://goo.gl/maps/Qs9pZABQC1ekVmh27',
      locationId: '28c510c3-cde7-4f97-ad3f-9e9a5f613eac'
    },
    {
      id: '427e17b9-8020-44e9-a08a-c8aefdcd15ff',
      name: 'Alba Futsal',
      description: 'Melayani sewa lapangan futsal dengan kualitas yang bagus.Dengan harga yang terjangkau dan pelayanan yang terbaik.Fasilitas lengkap dan tentunya harga sewa lapangan futsal di sini juga sangat bersahabat.Usaha kami mampu bersaing dengan usaha usaha lain yang sejenis dengan kami. Tapi tentunya kami yang terbaik. Kami mengedepankan kualitas dan kepuasan pelanggan.Tempat kami juga strategis mudah dicari dan selalu diingat.',
      price: '135000',
      address: 'Jl. Bintara No.6, RT.006/RW.013, Bintara, Kec. Bekasi Bar., Kota Bks, Jawa Barat 17134',
      addressUrl: 'https://goo.gl/maps/pjeXttGBBXRnroBKA',
      locationId: '28c510c3-cde7-4f97-ad3f-9e9a5f613eac'
    },
    {
      id: '7037d828-0cf2-43f9-9d6d-4b53509882df',
      name: 'Next Futsal',
      description: 'Melayani sewa lapangan futsal dengan kualitas yang bagus.Dengan harga yang terjangkau dan pelayanan yang terbaik.Fasilitas lengkap dan tentunya harga sewa lapangan futsal di sini juga sangat bersahabat.Usaha kami mampu bersaing dengan usaha usaha lain yang sejenis dengan kami. Tapi tentunya kami yang terbaik. Kami mengedepankan kualitas dan kepuasan pelanggan.Tempat kami juga strategis mudah dicari dan selalu diingat.',
      price: '140000',
      address: 'Jl. Inspeksi Saluran No.12B, RT.007/RW.9, Jakasampurna, Kec. Bekasi Bar., Kota Bks, Jawa Barat 17145',
      addressUrl: 'https://goo.gl/maps/oABPjw8Wjuk2uVTz9',
      locationId: '28c510c3-cde7-4f97-ad3f-9e9a5f613eac'
    }
    ])
  },

  down: async (queryInterface, Sequelize) => {
    await queryInterface.bulkDelete('fields', {});

  }
};