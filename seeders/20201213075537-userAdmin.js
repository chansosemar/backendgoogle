'use strict';

const { v4 } = require("uuid");

module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.bulkInsert('users', [{
      id: '1df4826a-cbf7-4f55-8706-91a41370f37a',
      fullname: 'admin',
      email: 'admin@kickin.com',
      password: 'admin123',
      photo: 'https://lh3.googleusercontent.com/5WamDDt34zE7ElhGFGZA1CR8bmxWlwBEDEdu0VrX2oP8wDIa5TcQWQcXsTGEnt48DIQ=s200',
      role: 'admin'
    },
    {
      id: '7ce6e740-2311-44f7-b95b-07cbeff63a2d',
      fullname: 'admin bumi',
      email: 'bumi@kickin.com',
      password: 'bumi123',
      photo: 'https://i.pinimg.com/564x/f9/46/19/f94619d8216e54199e8924f0c476d7aa.jpg',
      role: 'admin'
    }
    ])
  },

  down: async (queryInterface, Sequelize) => {
    await queryInterface.bulkDelete('users', {});
  }
};
