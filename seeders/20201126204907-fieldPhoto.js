'use strict';

const { v4 } = require("uuid");

module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.bulkInsert('fieldPhotos', [{
      id: v4(),
      fieldId: '0609e614-6ea7-478c-a73a-fa7b3bcff16c',
      photoUrl: 'https://wallpapercave.com/wp/wp2567605.jpg'
    },
    {
      id: v4(),
      fieldId: '0609e614-6ea7-478c-a73a-fa7b3bcff16c',
      photoUrl: 'https://wallpapercave.com/wp/wp2567607.jpg'
    },
    {
      id: v4(),
      fieldId: '0609e614-6ea7-478c-a73a-fa7b3bcff16c',
      photoUrl: 'https://wallpapercave.com/wp/wp2567603.jpg'
    },
    {
      id: v4(),
      fieldId: '0609e614-6ea7-478c-a73a-fa7b3bcff16c',
      photoUrl: 'https://wallpapercave.com/wp/wp2567601.jpg'
    },
    {
      id: v4(),
      fieldId: '0609e614-6ea7-478c-a73a-fa7b3bcff16c',
      photoUrl: 'https://wallpapercave.com/wp/wp1925375.jpg'
    },
    {
      id: v4(),
      fieldId: '2a0f4ead-263e-4a80-a39e-fb4213917602',
      photoUrl: 'https://wallpapercave.com/wp/wp1925378.jpg'
    },
    {
      id: v4(),
      fieldId: '2a0f4ead-263e-4a80-a39e-fb4213917602',
      photoUrl: 'https://thumbs.dreamstime.com/b/indoor-football-soccer-match-children-happy-kids-together-winning-futsal-game-chldren-celebrate-sport-victory-youth-s-108694951.jpg'
    },
    {
      id: v4(),
      fieldId: '2a0f4ead-263e-4a80-a39e-fb4213917602',
      photoUrl: 'https://thumbs.dreamstime.com/b/indoor-soccer-futsal-ball-goal-net-blue-background-football-168754980.jpg'
    },
    {
      id: v4(),
      fieldId: '2a0f4ead-263e-4a80-a39e-fb4213917602',
      photoUrl: 'https://thumbs.dreamstime.com/b/football-futsal-ball-youth-team-indoor-soccer-sports-hall-football-futsal-ball-youth-team-indoor-soccer-sports-hall-103323479.jpg'
    },
    {
      id: v4(),
      fieldId: '2a0f4ead-263e-4a80-a39e-fb4213917602',
      photoUrl: 'https://thumbs.dreamstime.com/b/football-futsal-training-children-indoor-soccer-young-player-ball-sports-hall-sport-background-80704981.jpg'
    },
    {
      id: v4(),
      fieldId: '427e17b9-8020-44e9-a08a-c8aefdcd15ff',
      photoUrl: 'https://thumbs.dreamstime.com/b/football-futsal-ball-goal-floor-indoor-soccer-sports-hall-sport-background-winter-league-tournament-103323204.jpg'
    },
    {
      id: v4(),
      fieldId: '427e17b9-8020-44e9-a08a-c8aefdcd15ff',
      photoUrl: 'https://thumbs.dreamstime.com/b/futsal-8819304.jpg'
    },
    {
      id: v4(),
      fieldId: '427e17b9-8020-44e9-a08a-c8aefdcd15ff',
      photoUrl: 'https://thumbs.dreamstime.com/b/little-boy-holding-football-futsal-gym-background-73119109.jpg'
    },
    {
      id: v4(),
      fieldId: '427e17b9-8020-44e9-a08a-c8aefdcd15ff',
      photoUrl: 'https://thumbs.dreamstime.com/b/max-era-pack-chrudim-futsal-25164618.jpg'
    },
    {
      id: v4(),
      fieldId: '427e17b9-8020-44e9-a08a-c8aefdcd15ff',
      photoUrl: 'https://thumbs.dreamstime.com/b/futsal-8982421.jpg'
    },
    {
      id: v4(),
      fieldId: '45fad555-1dc1-4379-99db-0fbe349caf0e',
      photoUrl: 'https://thumbs.dreamstime.com/b/indoor-soccer-sports-hall-football-futsal-player-ball-floor-background-youth-league-players-classic-127307517.jpg'
    },
    {
      id: v4(),
      fieldId: '45fad555-1dc1-4379-99db-0fbe349caf0e',
      photoUrl: 'https://thumbs.dreamstime.com/b/football-training-children-soccer-futsal-game-46403669.jpg'
    },
    {
      id: v4(),
      fieldId: '45fad555-1dc1-4379-99db-0fbe349caf0e',
      photoUrl: 'https://thumbs.dreamstime.com/b/children-playing-football-school-gymnasium-indoor-soccer-futsal-training-kids-player-ball-floor-cones-173109877.jpg'
    },
    {
      id: v4(),
      fieldId: '45fad555-1dc1-4379-99db-0fbe349caf0e',
      photoUrl: 'https://thumbs.dreamstime.com/b/futsal-junior-player-indoor-training-soccer-winter-class-school-court-young-play-field-161164548.jpg'
    },
    {
      id: v4(),
      fieldId: '45fad555-1dc1-4379-99db-0fbe349caf0e',
      photoUrl: 'https://thumbs.dreamstime.com/b/football-futsal-ball-goal-floor-indoor-soccer-sports-hall-sport-background-winter-league-168754961.jpg'
    },
    {
      id: v4(),
      fieldId: '669769e7-fdd9-49e1-8336-9567f7de3a19',
      photoUrl: 'https://thumbs.dreamstime.com/b/empty-soccer-field-spot-light-night-football-court-futsal-training-green-179170779.jpg'
    },
    {
      id: v4(),
      fieldId: '669769e7-fdd9-49e1-8336-9567f7de3a19',
      photoUrl: 'https://thumbs.dreamstime.com/b/football-field-futsal-green-grass-sport-outdoors-194360508.jpg'
    },
    {
      id: v4(),
      fieldId: '669769e7-fdd9-49e1-8336-9567f7de3a19',
      photoUrl: 'https://thumbs.dreamstime.com/b/indoor-soccer-player-training-balls-sports-hall-football-futsal-ball-floor-background-league-players-classic-127047856.jpg'
    },
    {
      id: v4(),
      fieldId: '669769e7-fdd9-49e1-8336-9567f7de3a19',
      photoUrl: 'https://thumbs.dreamstime.com/b/futsal-ball-court-13797086.jpg'
    },
    {
      id: v4(),
      fieldId: '669769e7-fdd9-49e1-8336-9567f7de3a19',
      photoUrl: 'https://thumbs.dreamstime.com/b/children-practice-futsal-park-182600913.jpg'
    },
    {
      id: v4(),
      fieldId: '6a84a2dc-4719-47bb-8e83-279148592004',
      photoUrl: 'https://thumbs.dreamstime.com/b/futsal-match-center-sydney-australia-127194411.jpg'
    },
    {
      id: v4(),
      fieldId: '6a84a2dc-4719-47bb-8e83-279148592004',
      photoUrl: 'https://thumbs.dreamstime.com/b/football-futsal-ball-wooden-floor-training-marker-running-player-cleats-indoor-soccer-sports-hall-sport-background-winter-199029179.jpg'
    },
    {
      id: v4(),
      fieldId: '6a84a2dc-4719-47bb-8e83-279148592004',
      photoUrl: 'https://thumbs.dreamstime.com/b/ball-hands-futsal-goalkeeper-wooden-floor-indoor-soccer-sports-hall-asian-player-background-youth-league-football-players-188456058.jpg'
    },
    {
      id: v4(),
      fieldId: '6a84a2dc-4719-47bb-8e83-279148592004',
      photoUrl: 'https://thumbs.dreamstime.com/b/futsal-amateurs-game-play-net-56195024.jpg'
    },
    {
      id: v4(),
      fieldId: '6a84a2dc-4719-47bb-8e83-279148592004',
      photoUrl: 'https://thumbs.dreamstime.com/b/seats-indoor-futsal-pitch-artificial-turf-substitutes-88967100.jpg'
    },
    {
      id: v4(),
      fieldId: '7037d828-0cf2-43f9-9d6d-4b53509882df',
      photoUrl: 'https://thumbs.dreamstime.com/b/futsal-goalkeeper-match-48533158.jpg'
    },
    {
      id: v4(),
      fieldId: '7037d828-0cf2-43f9-9d6d-4b53509882df',
      photoUrl: 'https://thumbs.dreamstime.com/b/aerial-view-soccer-futsal-field-local-185359317.jpg'
    },
    {
      id: v4(),
      fieldId: '7037d828-0cf2-43f9-9d6d-4b53509882df',
      photoUrl: 'https://thumbs.dreamstime.com/b/futsal-max-martin-hrdina-czech-league-match-slavia-prague-era-pack-chrudim-played-prague-38642820.jpg'
    },
    {
      id: v4(),
      fieldId: '7037d828-0cf2-43f9-9d6d-4b53509882df',
      photoUrl: 'https://thumbs.dreamstime.com/b/playing-field-futsal-21370562.jpg'
    },
    {
      id: v4(),
      fieldId: '7037d828-0cf2-43f9-9d6d-4b53509882df',
      photoUrl: 'https://thumbs.dreamstime.com/b/futsal-soccer-training-two-young-futsal-players-balls-training-close-up-legs-futsal-players-futsal-soccer-training-159821841.jpg'
    },
    {
      id: v4(),
      fieldId: '7f5b3efd-f157-4e3e-8db4-39d923ab7271',
      photoUrl: 'https://thumbs.dreamstime.com/b/futsal-field-photograph-old-style-antique-tone-futsal-field-photograph-old-style-antique-tone-159808389.jpg'
    },
    {
      id: v4(),
      fieldId: '7f5b3efd-f157-4e3e-8db4-39d923ab7271',
      photoUrl: 'https://thumbs.dreamstime.com/b/futsal-background-indoor-soccer-futsal-ball-team-sport-futsal-background-indoor-soccer-futsal-ball-team-sport-144535130.jpg'
    },
    {
      id: v4(),
      fieldId: '7f5b3efd-f157-4e3e-8db4-39d923ab7271',
      photoUrl: 'https://thumbs.dreamstime.com/b/futsal-training-picture-blurred-background-group-kids-indoor-soccer-children-physical-education-class-gym-youth-134284002.jpg'
    },
    {
      id: v4(),
      fieldId: '7f5b3efd-f157-4e3e-8db4-39d923ab7271',
      photoUrl: 'https://thumbs.dreamstime.com/b/futsal-field-photograph-old-style-antique-tone-futsal-field-photograph-old-style-antique-tone-159808358.jpg'
    },
    {
      id: v4(),
      fieldId: '7f5b3efd-f157-4e3e-8db4-39d923ab7271',
      photoUrl: 'https://thumbs.dreamstime.com/b/futsal-field-photograph-old-style-antique-tone-futsal-field-photograph-old-style-antique-tone-159808393.jpg'
    },
    {
      id: v4(),
      fieldId: '94e5e24f-1e10-4c68-aa93-8ffd34d8fe7c',
      photoUrl: 'https://thumbs.dreamstime.com/b/futsal-players-barefoot-player-control-shoot-ball-to-goal-soccer-fighting-each-other-kicking-indoor-sports-hall-162091907.jpg'
    },
    {
      id: v4(),
      fieldId: '94e5e24f-1e10-4c68-aa93-8ffd34d8fe7c',
      photoUrl: 'https://thumbs.dreamstime.com/b/aerial-view-futsal-basketball-court-surrounding-green-trees-aerial-view-futsal-basketball-court-123529859.jpg'
    },
    {
      id: v4(),
      fieldId: '94e5e24f-1e10-4c68-aa93-8ffd34d8fe7c',
      photoUrl: 'https://thumbs.dreamstime.com/b/futsal-friendly-match-poland-vs-serbia-lubin-december-action-darko-ristic-l-slide-tackle-ninoslav-aleksic-sebastian-173096887.jpg'
    },
    {
      id: v4(),
      fieldId: '94e5e24f-1e10-4c68-aa93-8ffd34d8fe7c',
      photoUrl: 'https://thumbs.dreamstime.com/b/futsal-field-goal-poles-green-background-41577669.jpg'
    },
    {
      id: v4(),
      fieldId: '94e5e24f-1e10-4c68-aa93-8ffd34d8fe7c',
      photoUrl: 'https://thumbs.dreamstime.com/b/green-football-court-futsal-empty-soccer-field-spot-light-night-179169449.jpg'
    },
    {
      id: v4(),
      fieldId: 'c4bf55da-3c2c-41c6-8e75-415cf0864986',
      photoUrl: 'https://www.dreamstime.com/mini-soccer-futsal-field-view-behind-goal-net-another-compititor-square-image181649946'
    },
    {
      id: v4(),
      fieldId: 'c4bf55da-3c2c-41c6-8e75-415cf0864986',
      photoUrl: 'https://thumbs.dreamstime.com/b/futsal-field-green-grass-sport-outdoor-indoor-football-soccer-195899996.jpg'
    },
    {
      id: v4(),
      fieldId: 'c4bf55da-3c2c-41c6-8e75-415cf0864986',
      photoUrl: 'https://thumbs.dreamstime.com/b/another-view-field-futsal-prior-to-match-outdoor-public-park-city-144155804.jpg'
    },
    {
      id: v4(),
      fieldId: 'c4bf55da-3c2c-41c6-8e75-415cf0864986',
      photoUrl: 'https://thumbs.dreamstime.com/b/playing-field-futsal-21370839.jpg'
    },
    {
      id: v4(),
      fieldId: 'c4bf55da-3c2c-41c6-8e75-415cf0864986',
      photoUrl: 'https://thumbs.dreamstime.com/b/corner-line-green-grass-futsal-field-football-white-192614150.jpg'
    },
    {
      id: v4(),
      fieldId: 'e222f356-5caa-4de8-9962-669291dcb0f3',
      photoUrl: 'https://thumbs.dreamstime.com/b/futsal-court-close-up-photo-taken-malaysia-158825191.jpg'
    },
    {
      id: v4(),
      fieldId: 'e222f356-5caa-4de8-9962-669291dcb0f3',
      photoUrl: 'https://thumbs.dreamstime.com/b/futsal-court-close-up-photo-taken-malaysia-158825004.jpg'
    },
    {
      id: v4(),
      fieldId: 'e222f356-5caa-4de8-9962-669291dcb0f3',
      photoUrl: 'https://thumbs.dreamstime.com/b/children-practice-futsal-park-182600899.jpg'
    },
    {
      id: v4(),
      fieldId: 'e222f356-5caa-4de8-9962-669291dcb0f3',
      photoUrl: 'https://thumbs.dreamstime.com/b/futsal-match-extra-league-fc-monolit-viva-cup-kardinal-rivne-kharkiv-ukraine-october-199237691.jpg'
    },
    {
      id: v4(),
      fieldId: 'e222f356-5caa-4de8-9962-669291dcb0f3',
      photoUrl: 'https://thumbs.dreamstime.com/b/futsal-court-futsal-court-close-up-photo-taken-malaysia-158825109.jpg'
    },
    {
      id: v4(),
      fieldId: 'eb0feb5c-cab9-42ca-8673-7fa7b5845b18',
      photoUrl: 'https://thumbs.dreamstime.com/b/futsal-match-extra-league-fc-monolit-viva-cup-kardinal-rivne-kharkiv-ukraine-october-199237659.jpg'
    },
    {
      id: v4(),
      fieldId: 'eb0feb5c-cab9-42ca-8673-7fa7b5845b18',
      photoUrl: 'https://thumbs.dreamstime.com/b/blurry-ball-futsal-player-control-to-shoot-goal-indoor-soccer-sports-hall-183897452.jpg'
    },
    {
      id: v4(),
      fieldId: 'eb0feb5c-cab9-42ca-8673-7fa7b5845b18',
      photoUrl: 'https://thumbs.dreamstime.com/b/football-field-small-futsal-ball-field-gym-indoor-soccer-sport-outdoor-park-artificial-turf-90815632.jpg'
    },
    {
      id: v4(),
      fieldId: 'eb0feb5c-cab9-42ca-8673-7fa7b5845b18',
      photoUrl: 'https://thumbs.dreamstime.com/b/indoor-soccer-sports-hall-football-futsal-player-ball-wooden-floor-background-youth-league-players-classic-187118357.jpg'
    },
    {
      id: v4(),
      fieldId: 'eb0feb5c-cab9-42ca-8673-7fa7b5845b18',
      photoUrl: 'https://thumbs.dreamstime.com/b/black-white-image-ball-hands-futsal-goalkeeper-wooden-floor-indoor-soccer-sports-hall-football-player-background-188097264.jpg'
    }
    ])
  },

  down: async (queryInterface, Sequelize) => {
    await queryInterface.bulkDelete('fieldPhotos', {});

  }
};