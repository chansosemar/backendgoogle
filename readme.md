# Welcome to Kickin API!

# Team Kickin [Back-End]
This project is part of Kickin App with other developers:
- Backend Team:
Novien, Giffari, Sudi
- Frontend Team:
Ari, Satrio, Deswanto
- React Native
Chandra, David

# About this project
Kickin is a one-stop platform for sports person especially futsal players, made with love and easy booking of futsal fields with best user interface experience. We believe there is an amazing opportunity to create great platform that bring people together. 

# Please read! [For Developer]
Please read this readme file first before you start coding.

## This starter project
This starter project already contains some basic library like:  
- Express
- Already installed Sequelize
- Already installed Mysql2
- Already installed Bycript
- Already installed Nodemon
  
## How to run the project
You need to install required dependencies (libraries) by typing in the terminal
```bash
npm install
```
Then you can run this project by:
- Using node
  ```bash
  npm start
  ```
- Using nodemon
  ```bash
  npm run dev
  ```
----------
## Working the feature
1. If you want to add a library, talk to me, now we use sequelize!
2. To run the database you can create a database in your local and run command migrate.
3. Use a git flow methodologies to create a feature, 
   Allowed branches are
   - master / main
   - feature with `feature-FEATURE_NAME` name scheme like `feature-authentication`, `feature-fields`
   - you can pull from `development`
  
----------
## Branching
Please create your own branch like: `feature-FEATURE_NAME`, also work per feature.  
The step is:  
- clone this repo
- git checkout `development`
- git branch `feature-FEATURE_NAME`
- git checkout `feature-FEATURE_NAME`

For example: creating authentication like login.  
make sure you are on `feature-authentication` branch!

- git branch `feature-authentication`
- git checkout `feature-authentication`

start coding...
if its all good, push.

- git add .
- git commit -m "Adding auth feature"
- git push origin `feature-authentication`

after create an auth feature then you want to create other feature, back to your `development` branch.

- git checkout `development`
- git branch `feature-fields`
- git checkout `feature-fields`

start coding...
if ok, push againn with:

- git add .
- git commit -m "Add feature fields"
- git push origin `feature-fields`

please back to `development` if you want to create other feature

## Descriptive commit message
Please use descriptive commit message for example:  
- git commit -m "Fixed bugs async storage not saved when logged in"
- git commit -m "Add home page"
- git commit -m "Refactor profie page"

DONT DO THIS:
- git commit -m "r"
- git commit -m "test"
- git commit -m "push hehe"
## Enviroment Variables
This project uses environment variables likes :
- DB_USERNAME=""
- DB_PASSWORD=""
- DB_DATABASE="kickin"
- DB_HOST="" 
- PORT=
- JWT_SECRET=""
- GOOGLE_CLIENT_ID= " "
- GOOGLE_CLIENT_SECRET= " "
- GMAIL_ACCOUNT=" "
- GMAIL_PASSWORD=" "
----------

## Coding style and guide
Here are some guide you must follow!
- Please use meaningful/proper variable name  
    for example, DONT USE THIS!:
    ```javascript
    const x = .....
    const n = .....
    ```
    Use like this:
    ```javascript
    const getRentData = ...
    const addTransaction = ....
    ```
- We are gonna separate the middleware/helper file
    **Dont** put your middleware, helper function in the route like below:
    ```javascript
    const  passport = require ('assport') 
    passport.use(new bearerStrategy(
    function (token, done) {

        })
    }))
    ```
    Please create a middleware/helper file for example, errorMiddleware.js
    ```javascript
    function errorHandler(err, req, res, next) {
  let errCode
  let errStatus
  let errMessage

  switch (err.name) {
    case 'SequelizeValidationError':
      errCode = 400;
      errStatus = err.name;
      errMessage = err.message
      break;
    }
    }
    module.exports = errorHandler

    //use that on component
    const errorHandler from 'errorHandler'
    const errorHandler = require('../../middleware/errorMiddleware')
    app.use (errorhandler)
    ```
    do the same for AsyncStorage!
----------

## Auto import function
Auto import can be used in all of controller, middleware, and helper functions

----------

## Database Diagram
https://dbdiagram.io/d/5fbd18323a78976d7b7d3a90

----------

## Postman Documentation
https://documenter.getpostman.com/view/13621678/TVmV6uDg

----------


