'use strict';
module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable('payments', {
      id: {
        type: Sequelize.UUID,
        primaryKey: true
      },
      transactionId: {
        type: Sequelize.UUID,
        allowNull: false,
        unique: true,
        references: {
          model: "transactions",
          key: "id"
        }
      },
      receiptPhoto: {
        type: Sequelize.STRING,
        allowNull: true,
      },
      isPaid: {
        type: Sequelize.BOOLEAN,
        allowNull: false
      },
      isAccepted: {
        type: Sequelize.BOOLEAN,
        allowNull: false
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable('payments');
  }
};