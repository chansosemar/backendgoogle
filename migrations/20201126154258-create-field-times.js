'use strict';
module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable('fieldTimes', {
      id: {
        type: Sequelize.UUID,
        primaryKey: true
      },
      fieldId: {
        type: Sequelize.UUID,
        allowNull: false,
        references: {
          model: "fields",
          key: "id"
        }
      },
      startDateTime: {
        allowNull: false,
        type: Sequelize.DATE
      },
      endDateTime: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable('fieldTimes');
  }
};