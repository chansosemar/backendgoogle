'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    return [
      await queryInterface.addColumn('users', 'phoneNumber', {
        type: Sequelize.TEXT,
        allowNull: true
      }),
      await queryInterface.addColumn('users', 'role', {
        type: Sequelize.TEXT,
        allowNull: false
      }),
      await queryInterface.addColumn('users', 'googleId', {
        type: Sequelize.TEXT,
        allowNull: true
      }),
    ]
  },

  down: async (queryInterface, Sequelize) => {
    await queryInterface.removeColumn('users', 'phoneNumber')
    await queryInterface.removeColumn('users', 'role')
    await queryInterface.removeColumn('users', 'googleId')
  }
};
