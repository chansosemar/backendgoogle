'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.addColumn('bookings', 'status', {
      allowNull: false,
      type: Sequelize.TEXT,
      defaultValue: "waiting for payment"
    })
  },

  down: async (queryInterface, Sequelize) => {
    await queryInterface.removeColumn('bookings', 'status')
  }
};
