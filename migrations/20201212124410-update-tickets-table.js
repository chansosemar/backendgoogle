'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.addColumn('tickets', 'paymentId', {
      type: Sequelize.UUID,
      allowNull: false,
      unique: true,
      reference: {
        model: "payments",
        key: "id"
      }
    })
  },

  down: async (queryInterface, Sequelize) => {
    await queryInterface.removeColumn('tickets', 'paymentId')
  }
};
