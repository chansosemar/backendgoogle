'use strict';
module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable('fieldPhotos', {
      id: {
        type: Sequelize.UUID,
        primaryKey: true
      },
      fieldId: {
        type: Sequelize.UUID,
        allowNull: false,
        references: {
          model: "fields",
          key: "id"
        }
      },
      photoUrl: {
        type: Sequelize.STRING,
        allowNull: false,
      }
    });
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable('field_photos');
  }
};