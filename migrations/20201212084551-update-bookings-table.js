'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.addColumn('bookings', 'isSuccessful', {
      type: Sequelize.BOOLEAN,
      allowNull: false
    })
  },


  down: async (queryInterface, Sequelize) => {
    await queryInterface.removeColumn('bookings', 'isSuccessful')
  }
};
