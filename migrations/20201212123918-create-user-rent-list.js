'use strict';
module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable('userRentLists', {
      id: {
        type: Sequelize.UUID,
        primaryKey: true
      },
      rentItemId: {
        type: Sequelize.UUID,
        allowNull: false,
        references: {
          model: "rentItems",
          key: "id"
        }
      },
      bookingId: {
        type: Sequelize.UUID,
        allowNull: false,
        references: {
          model: "bookings",
          key: "id"
        }
      },
      size: {
        type: Sequelize.STRING,
        allowNull: true,
      },
      quantity: {
        type: Sequelize.STRING,
        allowNull: true,
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable('userRentLists');
  }
};