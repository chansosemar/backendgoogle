const express = require('express')
const app = express.Router()
const passport = require('../middleware/authenticationMiddleware')
const BookingController = require('../controller/bookingController')
const { userRoleHandler } = require('../helper/roleHandler')
const errorHandler = require('../middleware/errorMiddleware')

app.post('/booking/:fields', passport.authenticate('bearer', { session: false }), BookingController.addBooking)
app.get('/booking/user/list', passport.authenticate('bearer', { session: false }), userRoleHandler, BookingController.getUserBookingHistory)
app.patch('/booking/:id', passport.authenticate('bearer', { session: false }), BookingController.editBooking)
app.get('/booking', BookingController.getFieldBookingSchedule)


app.use(errorHandler)
module.exports = app