const express = require('express')
const app = express.Router()
const passport = require('../middleware/authenticationMiddleware')
const errorHandler = require('../middleware/errorMiddleware')
const TicketController = require('../controller/ticketController')

app.get("/ticket/read", passport.authenticate('bearer', { session: false }), TicketController.getTicket)
app.post("/ticket/:id", passport.authenticate('bearer', { session: false }), TicketController.addTicket)

app.use(errorHandler)

module.exports = app