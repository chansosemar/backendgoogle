const express = require('express')
const app = express.Router()
const passport = require('../middleware/authenticationMiddleware')
const errorHandler = require('../middleware/errorMiddleware')
const upload = require('../middleware/uploadMiddleware')
const PaymentController = require('../controller/paymentController')

app.post("/payment/:id", passport.authenticate('bearer', { session: false }), upload.single('file'), PaymentController.addPayment)
app.patch("/payment/:id", passport.authenticate('bearer', { session: false }), upload.single('file'), PaymentController.editpayment)

app.use(errorHandler)
module.exports = app