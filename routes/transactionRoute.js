const express = require('express')
const app = express.Router()
const passport = require('../middleware/authenticationMiddleware')
const errorHandler = require('../middleware/errorMiddleware')
const TransactionController = require('../controller/transactionController')


app.post("/transaction/post/:bookingId", passport.authenticate('bearer', { session: false }), TransactionController.addTransaction)
app.get("/transaction/read/:bookingId", passport.authenticate('bearer', { session: false }), TransactionController.getTransaction)

app.use(errorHandler)

module.exports = app