const express = require('express')
const app = express.Router()
const passport = require('../middleware/authenticationMiddleware')
const errorHandler = require('../middleware/errorMiddleware')
const ReviewController = require('../controller/reviewController')

app.post("/review/post/:fieldId", passport.authenticate('bearer', { session: false }), ReviewController.addReview)
app.patch('/review/edit', passport.authenticate('bearer', { session: false }), ReviewController.editReview)
app.get("/review/read/:userId", passport.authenticate('bearer', { session: false }), ReviewController.getUserReviewHistory)

app.use(errorHandler)
module.exports = app