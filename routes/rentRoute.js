const express = require('express')
const RentController = require('../controller/rentController')
const passport = require('../middleware/authenticationMiddleware')
const errorHandler = require('../middleware/errorMiddleware')
const app = express.Router()


app.get('/rent/items', passport.authenticate('bearer', { session: false }), RentController.getRentItems)
app.post('/user/rent/list/:bookingId', passport.authenticate('bearer', { session: false }), RentController.addUserRentList)


app.use(errorHandler)

module.exports = app