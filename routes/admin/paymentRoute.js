const express = require('express')
const app = express.Router()
const errorHandler = require('../../middleware/errorMiddleware')
const passport = require('../../middleware/authenticationMiddleware')
const { adminRoleHandler } = require('../../helper/roleHandler')
const PaymentController = require('../../controller/paymentController')

app.get('/payment/admin/list', passport.authenticate('bearer', { session: false }),
    adminRoleHandler, PaymentController.getAllPayment)
app.patch('/payment/admin/list', passport.authenticate('bearer', { session: false }),
    adminRoleHandler, PaymentController.updatePaymentStatus)

app.use(errorHandler)

module.exports = app