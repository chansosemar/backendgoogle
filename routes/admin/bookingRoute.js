const express = require('express')
const app = express.Router()
const passport = require('../../middleware/authenticationMiddleware')
const errorHandler = require('../../middleware/errorMiddleware')
const { adminRoleHandler } = require('../../helper/roleHandler')
const BookingController = require('../../controller/bookingController')

app.patch('/booking/admin/list', passport.authenticate('bearer', { session: false }),
    adminRoleHandler, BookingController.updateBookingStatus)
app.get('/booking/admin/list', passport.authenticate('bearer', { session: false }),
    adminRoleHandler, BookingController.getAllBookingList)

app.use(errorHandler)

module.exports = app