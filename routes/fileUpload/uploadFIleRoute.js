const express = require('express')
const app = express.Router()
const passport = require('passport')
const errorHandler = require('../../middleware/errorMiddleware')
const upload = require('../../middleware/uploadMiddleware')

app.post('/files', passport.authenticate('bearer', { session: false }), upload.single('file'), async (req, res, next) => {
    try {
        const fileUrl = `${process.env.HOSTNAME}/files/${req.file.filename}`
        res.send(fileUrl)
    } catch (err) {
        next(err)
    }

})
app.use(errorHandler)
module.exports = app