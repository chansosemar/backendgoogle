const express = require('express')
const fs = require('fs')
const passport = require('passport')
const path = require('path')
const app = express.Router()


app.delete('/files', passport.authenticate('bearer', { session: false }), (req, res) => {
    if (!req.query.fileName) return res.status(400).send(" fileName key in the query required")
    fs.unlink(path.resolve('kickinUploads', req.query.fileName), (err) => {
        if (err)
            res.status(404).send('Not Found')
        else
            res.send('OK')
    })
})

module.exports = app