
const express = require('express');
const app = express.Router()
const FieldController = require('../controller/fieldController');
const errorHandler = require('../middleware/errorMiddleware');

app.get('/fields/read/all', FieldController.getAllFields);
app.get('/fields/location', FieldController.searchByLocation)
app.get('/fields/name', FieldController.searchByName)
app.get('/fields/info', FieldController.getFieldDetail)
app.get('/fields/photo', FieldController.getPhoto)
app.get('/fields/sort/name', FieldController.sortByName)
app.get('/fields/sort/price', FieldController.sortByPrice)

app.use(errorHandler)
module.exports = app