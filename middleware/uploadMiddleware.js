const multer = require('multer')
const fs = require('fs')
const path = require('path')

fs.readdir(path.resolve(), (err, files) => {
    if (err)
        console.log(err)
    else
        if (!files.includes('kickinUploads')) {
            console.log('creating kickinUploads folder');
            fs.mkdir(path.resolve('kickinUploads'), (err) => 1)
        }
})

const storage = multer.diskStorage({
    destination: function (req, file, cb) {
        cb(null, 'kickinUploads/')
    },
    filename: function (req, file, cb) {
        const fileName = file.originalname.toLowerCase().split(' ').join('-');
        cb(null, fileName)
    }
})

const upload = multer({
    storage: storage,
    fileFilter: (req, file, cb) => {
        if (file.mimetype == "image/png" || file.mimetype == "image/jpg" || file.mimetype == "image/jpeg") {
            cb(null, true);
        } else {
            cb(null, false);
            return cb(new Error('Only .png, .jpg and .jpeg format allowed!'));
        }
    }
})

module.exports = upload