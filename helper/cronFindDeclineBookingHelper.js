const { Op, fn, where, col, literal } = require('sequelize')
const db = require('../models')
const cron = require('node-cron')

const findDeclineBooking = cron.schedule("*/30 * * * *", async () => {
    const body = {
        status: "decline",
    }
    const updateData = await db.bookings.update(body, {
        // where(timestampdiff(HOUR, createdAt, NOW()) > 24) AND(isSuccessful: false)
        where: {
            [Op.and]: [
                where(fn('timestampdiff', literal('HOUR'), col('createdAt'), fn('NOW')), {
                    [Op.gt]: 24
                }),
                { isSuccessful: false }
            ]
        }
    })
    console.log('already decline');

})


module.exports = findDeclineBooking
