const chaiPromise = require('chai-as-promised')
const chai = require("chai")
chai.use(chaiPromise)
const { expect } = chai
const { address } = require("faker")
const { v4 } = require("uuid")
const DatabaseHelper = require("../databaseHelper")
const database = new DatabaseHelper()

let db
before(async () => {
    db = await database.start()
})

after(async () => {
    db = await database.drop()
})

const locationBody = {
    // id: v4(),
    location: address.city(),
}

beforeEach(async function () {
    this.newLocation = await db.locations.create({
        ...locationBody
    })
})

describe("model location", () => {

    it('should can add location', async function () {
        expect(this.newLocation.dataValues).to.have.property('id')
    })
    it('should can get user', async function () {
        const location = await db.locations.findOne({
            where: { id: this.newLocation.id }
        })
        expect(location).is.not.null
    })
    it('should can edit location', async function () {
        this.newLocation
        const newLocationName = address.city()
        const location = await db.locations.update({
            location: newLocationName
        },
            {
                where: { id: this.newLocation.id }
            })
        expect(location[0]).is.equal(1)
    })
    it('should can delete location', async function () {
        const location = await db.locations.destroy(
            { where: { id: this.newLocation.id } }
        )
        expect(location).is.equal(1)
    })
    it("should can't add location if no location", async function () {
        const cloneLocationBody = { ...this.newLocation }
        delete cloneLocationBody.dataValues.location
        await expect(db.locations.create(cloneLocationBody)).to.eventually.be.rejected
    })
})