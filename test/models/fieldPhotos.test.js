const chaiPromise = require('chai-as-promised')
const chai = require("chai")
chai.use(chaiPromise)
const { expect } = chai
const { address, lorem, company, commerce, image } = require("faker")
const DatabaseHelper = require("../databaseHelper")
const database = new DatabaseHelper()

let db
before(async () => {
    db = await database.start()
})

after(async () => {
    db = await database.drop()
})

const locationBody = {
    location: address.city()
}

const fieldBody = {
    name: company.companyName(),
    description: lorem.paragraph(1),
    price: commerce.price(),
    address: address.streetName(),
    addressUrl: address.direction()
}

const fieldPhotoBody = {
    photoUrl: image.imageUrl()
}

beforeEach(async function () {
    const newLocation = await db.locations.create(locationBody)
    const newField = await db.fields.create({
        name: company.companyName(),
        description: lorem.paragraph(1),
        price: commerce.price(),
        address: address.streetName(),
        addressUrl: address.direction(),
        locationId: newLocation.id
    })
    this.newFieldPhoto = await db.fieldPhotos.create({
        ...fieldPhotoBody,
        fieldId: newField.id
    })
})

describe("model fieldPhoto", () => {

    it('should can add fieldPhoto', async function () {
        expect(this.newFieldPhoto.dataValues).to.have.property('id')
    })
    it('should can get fieldPhoto', async function () {
        const fieldPhoto = await db.fieldPhotos.findOne({
            where: { id: this.newFieldPhoto.id }
        })
        expect(fieldPhoto).is.not.null
    })
    it('should can edit fieldPhoto', async function () {
        const newPhotoUrl = image.image()
        const fieldPhoto = await db.fieldPhotos.update({
            photoUrl: newPhotoUrl
        },
            {
                where: { id: this.newFieldPhoto.id }
            })
        expect(fieldPhoto[0]).is.equal(1)
    })
    it('should can delete fieldPhoto', async function () {
        const fieldPhoto = await db.fieldPhotos.destroy(
            { where: { id: this.newFieldPhoto.id } }
        )
        expect(fieldPhoto).is.equal(1)
    })
    it("should can't add fieldPhoto if no fieldId", async function () {
        const cloneFieldBody = { ...this.newFieldPhoto }
        delete cloneFieldBody.dataValues.fieldId
        await expect(db.fields.create(cloneFieldBody)).to.eventually.be.rejected
    })
})