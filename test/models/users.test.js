const chaiPromise = require('chai-as-promised')
const chai = require("chai")
chai.use(chaiPromise)
const { expect } = chai
const { random, name, internet } = require("faker")
const { v4 } = require("uuid")
const DatabaseHelper = require("../databaseHelper")
const database = new DatabaseHelper()

let db
before(async () => {
    db = await database.start()
})

after(async () => {
    db = await database.drop()
})

const userBody = {
    // id: v4(),
    fullname: name.firstName(),
    password: random.alphaNumeric(9)
}

beforeEach(async function () {
    this.newUser = await db.users.create({
        email: internet.email(),
        ...userBody
    })
})

describe("model user", () => {

    it('should can add user', async function () {
        expect(this.newUser.dataValues).to.have.property('id')
    })
    it('should can get user', async function () {
        const user = await db.users.findOne({
            where: { id: this.newUser.id }
        })
        expect(user).is.not.null
    })
    it('should can edit user', async function () {
        this.newUser
        const newFullName = name.firstName()
        const user = await db.users.update({
            fullname: newFullName
        },
            {
                where: { id: this.newUser.id }
            })
        expect(user[0]).is.equal(1)
    })
    it('should can delete user', async function () {
        const user = await db.users.destroy(
            { where: { id: this.newUser.id } }
        )
        expect(user).is.equal(1)
    })
    it("should can't add user if no fullname", async function () {
        const cloneUserBody = { ...this.newUser }
        delete cloneUserBody.dataValues.fullname
        await expect(db.users.create(cloneUserBody)).to.eventually.be.rejected
    })
    it("should can't add user if no email", async function () {
        const cloneUserBody = { ...this.newUser }
        delete cloneUserBody.dataValues.email
        await expect(db.users.create(cloneUserBody)).to.eventually.be.rejected
    })
    it("should can't add user if email is not email", async function () {
        this.newUser.email = name.firstName()
        await expect(db.users.create(this.newUser)).to.eventually.be.rejected
    })
    it("should can't add user if no password", async function () {
        const cloneUserBody = { ...this.newUser }
        delete cloneUserBody.dataValues.password
        await expect(db.users.create(cloneUserBody)).to.eventually.be.rejected
    })

})