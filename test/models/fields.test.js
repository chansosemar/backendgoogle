const chaiPromise = require('chai-as-promised')
const chai = require("chai")
chai.use(chaiPromise)
const { expect } = chai
const { address, lorem, company, commerce } = require("faker")
const DatabaseHelper = require("../databaseHelper")
const database = new DatabaseHelper()

let db
before(async () => {
    db = await database.start()
})

after(async () => {
    db = await database.drop()
})

const fieldBody = {
    name: company.companyName(),
    description: lorem.paragraph(1),
    price: commerce.price(),
    address: address.streetName(),
    addressUrl: address.direction()
}

const locationBody = {
    location: address.city()
}

beforeEach(async function () {
    const newLocation = await db.locations.create(locationBody)
    this.newField = await db.fields.create({
        ...fieldBody,
        locationId: newLocation.id
    })
})

describe("model field", () => {

    it('should can add field', async function () {
        expect(this.newField.dataValues).to.have.property('id')
    })
    it('should can get field', async function () {
        const field = await db.fields.findOne({
            where: { id: this.newField.id }
        })
        expect(field).is.not.null
    })
    it('should can edit field', async function () {
        const newFieldName = company.companyName()
        const field = await db.fields.update({
            name: newFieldName
        },
            {
                where: { id: this.newField.id }
            })
        expect(field[0]).is.equal(1)
    })
    it('should can delete field', async function () {
        const field = await db.fields.destroy(
            { where: { id: this.newField.id } }
        )
        expect(field).is.equal(1)
    })
    it("should can't add field if no locationId", async function () {
        const cloneFieldBody = { ...this.newField }
        delete cloneFieldBody.dataValues.locationId
        await expect(db.fields.create(cloneFieldBody)).to.eventually.be.rejected
    })
    it("should can't add field if no name", async function () {
        const cloneFieldBody = { ...this.newField }
        delete cloneFieldBody.dataValues.name
        await expect(db.fields.create(cloneFieldBody)).to.eventually.be.rejected
    })
    it("should can't add field if no price", async function () {
        const cloneFieldBody = { ...this.newField }
        delete cloneFieldBody.dataValues.price
        await expect(db.fields.create(cloneFieldBody)).to.eventually.be.rejected
    })
    it("should can't add field if no address", async function () {
        const cloneFieldBody = { ...this.newField }
        delete cloneFieldBody.dataValues.address
        await expect(db.fields.create(cloneFieldBody)).to.eventually.be.rejected
    })
    it("should can't add field if no addressUrl", async function () {
        const cloneFieldBody = { ...this.newField }
        delete cloneFieldBody.dataValues.addressUrl
        await expect(db.fields.create(cloneFieldBody)).to.eventually.be.rejected
    })
})