const dayjs = require("dayjs")
const { Op } = require("sequelize")
const nodemailer = require("nodemailer");

const db = require("../models")


class BookingController {

    static async addBooking(req, res, next) {
        try {
            const time = dayjs()
            const cekStartTime = Date.parse(req.body.startTime) < Date.parse(time.$d)
            const cekEndTime = Date.parse(req.body.endTime) < Date.parse(time.$d)

            // cari semua data waktu di lapangan itu
            const data = await db.fieldTimes.findAll({
                where: {
                    fieldId: req.params.fields,
                    endDateTime: { [Op.gt]: dayjs() }
                },
                include: db.fields
            })
            if (data == 0) {
                return res.status(404).send('this field no longer operate')
            }
            const operasionalOpenTime = Date.parse(req.body.startTime) >
                Date.parse(data[0].dataValues.endDateTime)
            const operasionalCloseTime = Date.parse(req.body.endTime) >
                Date.parse(data[0].dataValues.endDateTime)

            //untuk mendapatkan data booking di satu lapangan
            const getBookingListOnFields = await db.bookings.findAll({
                where: {
                    [Op.and]: [
                        {
                            fieldTimeId: data[0].dataValues.id
                        },
                        {
                            startTime: {
                                [Op.between]: [req.body.startTime,
                                (Date.parse(req.body.endTime) - 60000)
                                ]
                            }
                        },
                        {
                            [Op.or]: [
                                {
                                    status: "waiting for payment",
                                },
                                {
                                    status: "waiting for confirmation",
                                },
                                {
                                    status: "accepted",
                                },
                            ]
                        }
                    ]
                }
            })

            //untuk mengecek data booking user, agar tidak double book
            const getBookingListOfUser = await db.bookings.findAll({
                where:
                {
                    startTime: req.body.startTime,
                    endTime: req.body.endTime,
                    userId: req.user.id,
                    [Op.or]: [
                        {
                            status: "waiting for payment",
                        },
                        {
                            status: "waiting for confirmation",
                        },
                        {
                            status: "accepted",
                        },
                    ]
                },
            })

            if (getBookingListOnFields.length > 0 || getBookingListOfUser.length > 0) {
                return res.status(409).send('there is another booking at field/you have another booking schedule at that time');
            } else if (cekStartTime || cekEndTime) {
                return res.status(406).send('Not acceptable');
            } else if (operasionalOpenTime || operasionalCloseTime) {
                return res.status(406).send('Not acceptable, more than fields operational time');
            } else if (req.body.startTime > req.body.endTime) {
                return res.status(406).send('Not acceptable');
            } else {
                const id = data[0].dataValues.id
                const booking = await db.bookings.create({
                    userId: req.user.id,
                    startTime: req.body.startTime,
                    endTime: req.body.endTime,
                    fieldTimeId: id
                })

                res.status(200).json({
                    message: 'here your booking data :',
                    data: {
                        booking
                    }
                })
            }
        } catch (err) {
            next(err);
        }
    }

    static async editBooking(req, res, next) {
        try {
            const getbooking = await db.bookings.findAll({
                where: {
                    id: req.params.id
                }
            })

            if (getbooking.length === 0) {
                return res.status(404).send("Id not found")
            }
            // //** menginputkan data ke database */
            const updatedBookings = await db.bookings.update(req.body, {
                where: {
                    id: req.params.id,
                }
            })
            if (updatedBookings.length == 0) {
                res.send("There is no data updated")
            }
            else {
                const getDataUpdated = await db.bookings.findOne({
                    where: {
                        id: req.params.id
                    }
                })
                res.send(getDataUpdated)
            }
        } catch (err) {
            next(err);
        }
    }

    static async getFieldBookingSchedule(req, res, next) {
        try {
            const getFieldData = await db.fields.findOne({
                where: { id: req.query.id },
                include: db.fieldTimes,
            })
            const parseFieldData = JSON.parse(JSON.stringify(getFieldData))

            const getBookingListToday = await db.bookings.findAll({
                order: [
                    'startTime'
                ],
                where: {
                    [Op.and]: [{
                        fieldTimeId: parseFieldData.fieldTimes[0].id
                    },
                    {
                        //startTime: { [Op.lt]: dayjs() } // <= kemaren dan seterusnya
                        startTime: { [Op.gt]: dayjs() },// => hari ini dari jam 00:00 dan seterusnya,
                    },
                    {
                        [Op.or]: [
                            {
                                status: "waiting for payment",
                            },
                            {
                                status: "waiting for confirmation",
                            },
                            {
                                status: "accepted",
                            },
                        ]
                    }]
                }
            })

            if (getBookingListToday.length === 0) {
                res.status(200).send('There is no bookinglist today')
            } else {
                res.status(200).json({
                    message: 'Available Time :',
                    data: {
                        getFieldData,
                        getBookingListToday
                    }
                })
            }
        } catch (err) {
            next(err);
        }
    }

    static async getUserBookingHistory(req, res, next) {
        try {
            const getUserBooking = await db.bookings.findAll({
                order: [
                    'startTime'
                ],
                where: { userId: req.user.id },
                include: [
                    {
                        model: db.fieldTimes,
                        include: {
                            model: db.fields,
                        }
                    },
                    {
                        model: db.transactions, attributes: ['id', 'totalPrice'],
                        include: {
                            model: db.payments
                        }
                    }

                ]

            })

            res.status(200).json({
                message: 'All user booking List :',
                data: {
                    getUserBooking
                }
            })
        } catch (err) {
            next(err);
        }
    }

    static async getAllBookingList(req, res, next) {
        try {
            const getBookingListToday = await db.bookings.findAll({
                where: {
                    startTime: { [Op.gt]: dayjs() }, // => hari ini dari jam 00:00 dan seterusnya
                    status: 'waiting for confirmation'
                },
                include:
                    [{
                        model: db.fieldTimes,
                        include: [{
                            model: db.fields, attributes: ['name']
                        }]
                    }, {
                        model: db.users, attributes: ['fullname', 'email']
                    }]

            })

            if (getBookingListToday.length === 0) {
                res.status(404).send('There is no bookinglist today')
            } else {
                res.status(200).json({
                    message: 'Available Time :',
                    data: {
                        getBookingListToday
                    }
                })
            }

        } catch (err) {
            next(err)
        }
    }

    static async updateBookingStatus(req, res, next) {

        try {
            const { isSuccessfull, status } = req.body
            req.body.status = "accepted"
            const getbooking = await db.bookings.findAll({
                where: {
                    id: req.query.id
                }
            })

            if (getbooking.length === 0) {
                return res.status(404).send("Id not found")
            } else {
                // //** menginputkan data ke database */
                await db.bookings.update(req.body, {
                    where: {
                        id: req.query.id
                    }
                })
            }

            const result = await db.bookings.findOne({
                where: {
                    id: req.query.id
                },
                include: db.fieldTimes
            });

            const parseResult = JSON.parse(JSON.stringify(result))
            const fieldDetails = await db.fields.findOne({
                attributes: ["name", "address"],
                where: {
                    id: parseResult.fieldTime.fieldId,
                },
            });

            const userDetails = await db.users.findOne({
                attributes: ["fullname", "email"],
                where: {
                    id: parseResult.userId,
                },
            });

            const bookingDetails = parseResult
            Object.assign(bookingDetails, {
                fieldDetails: {
                    name: fieldDetails.dataValues.name,
                    address: fieldDetails.dataValues.address,
                },
                userDetails: {
                    name: userDetails.dataValues.fullname,
                    email: userDetails.dataValues.email
                },
            });
            const transporter = nodemailer.createTransport({
                service: "gmail",
                auth: {
                    user: process.env.GMAIL_ACCOUNT,
                    pass: process.env.GMAIL_PASSWORD,
                },
            });
            const html = `
            <h3>Dear Mr./Mrs. ${bookingDetails.userDetails.name}</h3>
            <p>We would like to inform you that your booking with below details:</p>
            <ul style="list-style-type: none">
                <li>Clinic Name: ${bookingDetails.fieldDetails.name}</li>
                <li>Clinic Address: ${bookingDetails.fieldDetails.address}</li>
            </ul>
            <p>Has been ${bookingDetails.status}</p>
        `;
            const mailOptions = {
                from: process.env.GMAIL_ACCOUNT,
                to: bookingDetails.userDetails.email,
                subject: `Your Booking Status for booking id: ${bookingDetails.id}`,
                html,
            };

            transporter.sendMail(mailOptions, (err, info) => {
                if (err) {
                    console.log(err);
                } else {
                    console.log("Email Sent: " + info.response);
                }
            });

            res.status(200).json(bookingDetails);
        } catch (err) {
            next(err)
        }
    }

}


module.exports = BookingController;