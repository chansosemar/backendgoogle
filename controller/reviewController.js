const { Op } = require("sequelize");
const db = require("../models")

class ReviewController {

    static async getUserReviewHistory(req, res, next) {

        try {
            const userId = req.user.id
            const getReview = await db.reviews.findAll({
                where: {
                    userId
                },
                include: [
                    { model: db.users },
                    { model: db.fields },
                ]
            })

            if (getReview.length > 0) {
                res.status(200).send(getReview);
            } else if (getReview.length === 0) {
                res.status(200).send([])
            }
            else {
                res.status(404).send("Data is not found");
            }
        } catch (err) {
            next(err);
        }
    }

    static async addReview(req, res, next) {
        try {
            const body = req.body
            body.userId = req.user.id
            body.fieldId = req.params.fieldId

            const review = await db.reviews.findAll({
                where: {
                    [Op.and]: [
                        { userId: body.userId },
                        {
                            fieldId: body.fieldId
                        },
                    ]
                }
            })

            if (review.length > 0) {
                return res.status(409).send('You already write a review at this field')
            }
            else {
                const addReview = await db.reviews.create(body)

                if (addReview) {
                    const result = await db.reviews.findOne({
                        where: {
                            [Op.and]: [
                                { userId: body.userId },
                                {
                                    fieldId: body.fieldId
                                },
                            ]
                        }
                    })
                    res.status(200).send(result);
                } else {
                    res.status(400).send("Wrong Body");
                }
            }
        } catch (err) {
            next(err);
        }
    }

    static async editReview(req, res, next) {
        try {
            let query = req.query
            let body = req.body
            const getReview = await db.reviews.findAll({
                where: {
                    id: query.id
                }
            }).catch((err) => next(err))
            if (getReview.length === 0) {
                return res.status(409).send("Id not found")
            }
            // //** menginputkan data ke database */
            const updatedReview = await db.reviews.update(body, {
                where: {
                    id: query.id
                }
            })
            if (updatedReview.length == 0) {
                res.send("There is no data updated")
            }
            else {
                const getDataUpdated = await db.reviews.findAll({
                    where: {
                        id: query.id
                    }
                })
                res.send(getDataUpdated)
            }

        } catch (err) {
            next(err);
        }
    }

}


module.exports = ReviewController;