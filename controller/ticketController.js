const db = require("../models")


class TicketController {

    static async addTicket(req, res, next) {
        try {
            const getTransactionData = await db.payments.findOne({
                include: [{
                    model: db.transactions,
                    where: {
                        bookingId: req.params.id,
                    },
                }],
            })
            const idPayment = getTransactionData.dataValues.id
            if (getTransactionData) {
                const addTicket = await db.tickets.create({
                    bookingId: req.params.id,
                    paymentId: idPayment
                })
                if (addTicket) {
                    const result = await db.tickets.findOne({
                        where: {
                            bookingId: req.params.id,
                            paymentId: idPayment
                        },
                        include: [{
                            model: db.bookings, attributes: ['startTime', 'endTime', 'status'],
                            include: [{
                                model: db.users, attributes: ['fullname']
                            },
                            {
                                model: db.fieldTimes,
                                include: [{
                                    model: db.fields, attributes: ['name', 'price', 'address', 'addressUrl']
                                }]
                            }]
                        },
                        {
                            model: db.payments,
                        }
                        ],
                    })
                    return res.status(200).send(result);
                }
            } else {
                return res.status(404).send('data not found')
            }
        } catch (err) {
            next(err);
        }
    }

    static async getTicket(req, res, next) {
        try {
            const getTicket = await db.bookings.findAll({
                where: {
                    id: req.query.q
                },

                include: [{
                    model: db.users, attributes: ['fullname']
                },
                {
                    model: db.fieldTimes,
                    include: [{
                        model: db.fields, attributes: ['name', 'price', 'address', 'addressUrl']
                    }]
                }]
            })

            if (getTicket.length > 0) {
                res.status(200).send(getTicket);
            } else {
                res.status(404).send("Data is not found");
            }
        } catch (err) {
            next(err);
        }
    }

}


module.exports = TicketController;