const { Op } = require("sequelize");
const db = require("../models")


class FieldController {

    static async getAllFields(req, res, next) {
        try {
            const fields = await db.fields.findAll({

                include: db.fieldPhotos
            })
            res.send(fields)
        } catch (err) {
            next(err);
        }
    }

    static async searchByLocation(req, res, next) {
        try {
            const fields = await db.locations.findAll({
                where: {
                    location: {
                        [Op.like]: '%' + req.query.q + '%'
                    }
                },
                include:
                {
                    model: db.fields,
                    include: db.fieldPhotos
                }

            })
            if (fields.length > 0) {
                res.send(fields)
            } else {
                res.status(404).send('data not found')
            }
        } catch (err) {
            next(err);
        }
    }

    static async searchByName(req, res, next) {
        try {
            const fields = await db.fields.findAll({
                where: {
                    name: {
                        [Op.like]: '%' + req.query.q + '%'
                    }
                },
                include: db.fieldPhotos
            })
            if (fields.length > 0) {
                res.send(fields)
            } else {
                res.status(404).send('data not found')
            }
        } catch (err) {
            next(err);
        }
    }

    static async getFieldDetail(req, res, next) {
        try {
            const fieldDetail = await db.fields.findOne({
                where: { id: req.query.id },
                include: [{
                    model: db.fieldPhotos,
                    // limit:1
                },
                {
                    model: db.reviews,
                    attributes: ['comment', 'rating'],
                    include: {
                        model: db.users,
                        attributes: ['fullname']
                    }
                }
                ]

            })
            if (fieldDetail) {
                const result = JSON.parse(JSON.stringify(fieldDetail))

                let avg = result.reviews.map(i => i.rating)
                avg = avg.reduce((a, b) => a + b, 0) / avg.length
                result.average = avg
                res.send(result)
            } else {
                res.status(404).send('data not found')
            }
        } catch (err) {
            next(err);
        }
    }

    static async getPhoto(req, res, next) {
        try {
            const fieldPhotos = await db.fieldPhotos.findAll({

                where: {
                    id: req.query.id
                }
            })
            res.send(fieldPhotos)
        } catch (err) {
            next(err);
        }
    }

    static async sortByName(req, res, next) {
        try {
            const fields = await db.fields.findAll({
                order: [
                    'name'
                ],
                include: db.fieldPhotos
            })
            res.send(fields)
        } catch (err) {
            next(err);
        }
    }

    static async sortByPrice(req, res, next) {
        try {
            const fields = await db.fields.findAll({
                order: [
                    'price'
                ],
                include: db.fieldPhotos
            })
            res.send(fields)
        } catch (err) {
            next(err);
        }
    }

}


module.exports = FieldController;