const jwt = require('jsonwebtoken')

const secret = process.env.JWT_SECRET

const { salt, checkPassword } = require('../helper/bycryptHelper');
const db = require('../models');
const verify = require('../helper/googleHelper');

class AuthController {
    static async register(req, res, next) {
        try {
            const { fullname, email, phoneNumber, password } = req.body;
            const hashPassword = await salt(password).catch(err => {
                return res.status(500).send('password encryption failed')
            })
            const user = await db.users.create({
                fullname,
                email,
                phoneNumber,
                password: hashPassword,
                role: 'user',
            })

            delete user.dataValues.password
            res.send(user)
        } catch (err) {
            next(err);
        }
    }

    static async login(req, res, next) {
        try {
            const body = req.body
            let getUser = await db.users.findAll({
                where: {
                    email: body.email
                }
            })

            if (getUser.length === 0) {
                res.status(404).send("User is not available")
            }
            else {
                const user = getUser[0]
                if (user.dataValues.role === 'admin') {
                    if (body.password === user.dataValues.password) {
                        const token = jwt.sign(user.dataValues, secret, {
                            expiresIn: '365d'
                        })
                        user.dataValues.token = token
                        delete user.dataValues.password
                        return res.send(user.dataValues)
                    } else {
                        res.status(400).send("Wrong Password")
                    }
                } else {
                    const isPassMatch = await checkPassword(body.password, user.dataValues.password)
                    if (!isPassMatch) {
                        res.status(400).send("Wrong Password, Please don't be account mafia :D")
                    } else {
                        const token = jwt.sign(user.dataValues, secret, {
                            expiresIn: '365d'
                        })
                        user.dataValues.token = token
                        delete user.dataValues.password
                        res.send(user.dataValues)
                    }
                }
            }
        } catch (err) {
            next(err);
        }
    }

    static async googleRegister(req, res, next) {
        try {
            const { token } = req.body;
            const googleAuth = await verify(token) //.catch((err) => { next({ name: 'GOOGLE_ERROR', message: err }) });

            if (googleAuth) {
                const allProfile = `${googleAuth.azp}${googleAuth.email}${googleAuth.iat}`

                const hashPayload = await salt(allProfile).catch(err => {
                    return res.status(500).send('payload encryption failed')
                })

                if (googleAuth) {
                    const user = await db.users.create({
                        fullname: googleAuth.name,
                        email: googleAuth.email,
                        password: hashPayload,
                        photo: googleAuth.picture,
                        googleId: googleAuth.sub,
                        role: 'user'
                    })
                    return res.send(user)

                } else {
                    next(null, false)
                }
            }

        } catch (err) {
            next(err);
        }
    }

    static async googleLogin(req, res, next) {
        try {
            const { token } = req.body;
            const googleAuth = await verify(token).catch((err) => { next({ name: 'GOOGLE_ERROR', message: err }) });

            if (googleAuth) {
                let getUser = await db.users.findAll({
                    where: {
                        email: googleAuth.email,
                        googleId: googleAuth.sub
                    }
                })

                const user = getUser[0]
                const token = jwt.sign({ body: user }, secret, {
                    expiresIn: '365d'
                })

                user.dataValues.token = token
                delete user.dataValues.password
                res.send(user.dataValues)
            } else {
                res.status(404).send("sorry,you haven't use your google account on our app")
            }

        } catch (err) {
            next(err);
        }
    }


}


module.exports = AuthController;
