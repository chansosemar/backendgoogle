const db = require("../models")


class TransactionController {

    static async addTransaction(req, res, next) {
        try {
            const getUserRentList = await db.userRentLists.findAll({
                where: {
                    bookingId: req.params.bookingId
                },
                include: db.rentItems
            })

            var totalKitPrice = 0
            for (let i = 0; i < getUserRentList.length; i++) {
                const result = JSON.parse(JSON.stringify(getUserRentList[i]))
                const kitPrice = (result.quantity) * (result.rentItem.price)
                totalKitPrice += kitPrice
            }
            const rentKit = totalKitPrice


            const getBookingPrice = await db.bookings.findOne({
                where: {
                    id: req.params.bookingId
                },
                include: {
                    model: db.fieldTimes,
                    include: db.fields
                }
            })

            const hasil = JSON.parse(JSON.stringify(getBookingPrice))
            const totalHour = (Date.parse(hasil.endTime) - Date.parse(hasil.startTime)) / 3600000
            const fieldPrice = totalHour * hasil.fieldTime.field.price


            const transactionData = await db.transactions.create({
                bookingId: req.params.bookingId,
                totalPrice: rentKit + fieldPrice
            })
            if (transactionData) {
                const getFieldData = await db.bookings.findOne({
                    where: {
                        id: req.params.bookingId
                    },
                    attributes: ['startTime', 'endTime'],
                    include: {
                        model: db.fieldTimes,
                        include: {
                            model: db.fields, attributes: ['name', 'address', 'price']
                        }
                    }
                })

                const getRentData = await db.userRentLists.findAll({
                    where: {
                        bookingId: req.params.bookingId
                    },
                    attributes: ['size', 'quantity'],
                    include: [{
                        model: db.rentItems, attributes: ['name', 'price']
                    }
                    ]
                })
                if (!getRentData) {
                    res.status(200).json({
                        message: 'here your transaction data :',
                        data: {
                            getFieldData,
                            transactionData,
                        }
                    })
                } else {
                    res.status(200).json({
                        message: 'here your transaction data :',
                        data: {
                            getFieldData,
                            getRentData,
                            transactionData,
                        }
                    })
                }
            }
        } catch (err) {
            next(err);
        }
    }

    static async getTransaction(req, res, next) {
        try {
            const getUserRentList = await db.userRentLists.findAll({
                where: {
                    bookingId: req.params.bookingId
                },
                include: db.rentItems
            })
            var kit = []
            for (let i = 0; i < getUserRentList.length; i++) {
                const result = JSON.parse(JSON.stringify(getUserRentList[i]))
                const kitPrice = (result.quantity) * (result.rentItem.price)
                kit.push(kitPrice)
            }

            const getBookingPrice = await db.bookings.findOne({
                where: {
                    id: req.params.bookingId
                },
                include: {
                    model: db.fieldTimes,
                    include: db.fields
                }
            })

            const hasil = JSON.parse(JSON.stringify(getBookingPrice))
            const totalHour = (Date.parse(hasil.endTime) - Date.parse(hasil.startTime)) / 3600000
            const fieldPrice = totalHour * hasil.fieldTime.field.price

            const getFieldData = await db.bookings.findOne({
                where: {
                    id: req.params.bookingId
                },
                attributes: ['startTime', 'endTime'],
                include: {
                    model: db.fieldTimes, attributes: ['id'],
                    include: {
                        model: db.fields, attributes: ['name', 'address', 'price']
                    }
                }
            })
            getFieldData.dataValues.fieldPrice = fieldPrice

            const getRentData = await db.userRentLists.findAll({
                where: {
                    bookingId: req.params.bookingId
                },
                attributes: ['size', 'quantity'],
                include: [{
                    model: db.rentItems, attributes: ['name', 'price']
                }
                ]
            })
            for (let i = 0; i < getRentData.length; i++) {
                getRentData[i].dataValues.totalPrice = kit[i]
            }
            if (!getRentData) {
                return res.status(200).json({
                    message: 'here your transaction data :',
                    data: {
                        getFieldData,
                    }
                })
            }
            else {
                return res.status(200).json({
                    message: 'here your transaction data :',
                    data: {
                        getFieldData,
                        getRentData,
                    }
                })
            }
        } catch (err) {
            next(err);
        }
    }


}


module.exports = TransactionController;