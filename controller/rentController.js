

const db = require("../models")


class RentController {

    static async getRentItems(req, res, next) {
        try {
            const items = await db.rentItems.findAll({
                where: req.query
            })
            res.send(items)
        } catch (err) {
            next(err);
        }
    }

    static async addUserRentList(req, res, next) {
        try {
            const body = req.body
            body.bookingId = req.params.bookingId
            body.rentItemId = req.query.id

            const rentLists = await db.userRentLists.findOne({
                where: {
                    rentItemId: req.query.id,
                    bookingId: req.params.bookingId
                }
            })

            if (rentLists) {
                return res.status(409).send('you already add this item')
            } else {
                const addList = await db.userRentLists.create(body)
                if (addList) {
                    const result = await db.userRentLists.findAll({
                        where:
                        {
                            bookingId: req.params.bookingId
                        },

                    })
                    return res.status(200).send(result);
                } else {
                    return res.status(400).send("Wrong Body");
                }
            }
        } catch (err) {
            next(err);
        }

    }

}


module.exports = RentController;