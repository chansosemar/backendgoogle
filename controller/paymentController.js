const dayjs = require("dayjs")
const { Op } = require("sequelize")
const db = require("../models")

class PaymentController {

    static async addPayment(req, res, next) {
        try {
            if (req.file) {
                const addPayment = await db.payments.create({
                    transactionId: req.params.id,
                    receiptPhoto: `${process.env.HOSTNAME}/files/${req.file.filename}`,
                    isPaid: true
                })
                if (addPayment) {
                    const getTransaction = await db.transactions.findOne({
                        where: {
                            id: req.params.id
                        }
                    })
                    const body = {
                        status: "waiting for confirmation"
                    }
                    const updateBookingStatus = await db.bookings.update(body, {
                        where: {
                            id: getTransaction.dataValues.bookingId
                        }
                    })

                    if (updateBookingStatus.length === 0) {
                        return res.send('There is no data updated')
                    } else {
                        const findUpdatedBookingstatus = await db.bookings.findOne({
                            where: {
                                id: getTransaction.dataValues.bookingId
                            },
                            include: {
                                model: db.transactions,
                                include: db.payments
                            }
                        })
                        res.status(200).send(findUpdatedBookingstatus)

                    }
                }
            } else {
                return res.send('please upload you payment')
            }
        } catch (err) {
            next(err);
        }
    }

    static async editpayment(req, res, next) {
        try {
            const findTransaction = await db.payments.findAll({
                where: {
                    transactionId: req.params.id
                }
            })
            const idPayment = findTransaction[0]

            if (findTransaction.length > 0 && req.file) {
                const body = {
                    receiptPhoto: `${process.env.HOSTNAME}/files/${req.file.filename}`,
                    isPaid: true,
                }
                const updatedPayment = await db.payments.update(body, {
                    where: {
                        id: idPayment.dataValues.id
                    }
                })
                if (updatedPayment.length === 1) {
                    const editedPaymentStatus = await db.payments.findOne({
                        where: {
                            transactionId: req.params.id
                        }
                    })
                    return res.send(editedPaymentStatus)
                }
            }
            else {
                res.status(404).send('data not found')
            }
        } catch (err) {
            next(err);
        }
    }

    static async getAllPayment(req, res, next) {
        try {

            const now = dayjs()
            const zoneTime = now.add('1', 'day')
            const oneDayBefore = now.subtract('1', 'day')

            const getpaymentListToday = await db.payments.findAll({
                where: {
                    [Op.and]: [
                        {
                            isPaid: true
                        },
                        {
                            createdAt: {
                                [Op.between]: [
                                    oneDayBefore.$d, zoneTime.$d
                                ]
                            }
                        }
                    ]
                },
                include: {
                    model: db.transactions,
                    include: {
                        model: db.bookings,
                        include: {
                            model: db.fieldTimes,
                            include: db.fields
                        }
                    }
                }
            })
            if (getpaymentListToday.length === 0) {
                res.status(404).send('someone has not pay it yet')
            } else {
                res.status(200).json({
                    message: 'Payment List :',
                    data: {
                        getpaymentListToday
                    }
                })
            }
        } catch (err) {
            next(err)
        }
    }

    static async updatePaymentStatus(req, res, next) {
        let query = req.query
        const { isAccepted } = req.body

        try {
            const getPayment = await db.payments.findAll({
                where: {
                    id: query.id
                }
            }).catch((err) => next(err))
            if (getPayment.length === 0) {
                return res.status(404).send("Id not found")
            }
            // //** menginputkan data ke database */
            const updatedPayments = await db.payments.update(req.body, {
                where: {
                    id: query.id
                }
            })
            if (updatedPayments.length == 0) {
                res.send("There is no data updated")
            }
            else {
                const getDataUpdated = await db.payments.findOne({
                    where: {
                        id: query.id
                    }
                })
                res.send(getDataUpdated)
            }

        } catch (err) {
            next(err)
        }
    }




}


module.exports = PaymentController;